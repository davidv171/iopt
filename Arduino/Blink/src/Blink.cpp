#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/
/*Stuff to look out for: 256KB flash memory(8KB for bootloader) 8KB of SRAM and 4KB of EEPROM

  USE OF INTERRUPTS TO CANCEL DELAYS??
  BLUETOOTH DEFAULT PIN : 123456
*/
// MPU-6050 Short Example Sketch
// By Arduino User JohnChi
// August 17, 2014
// Public Domain
// the setup function runs once when you press reset or power the board
//---------------
//---------------
const int8_t BUZZER = 9; //BUZZER to arduino pin 9
const int8_t BUZZER2 = 10;
const int8_t STOP_BUTTON = 11;
const int8_t YELLOW_LIGHT = 12;
const int8_t MPU_ADDR = 0x68; // I2C address of the MPU-6050
void setup()
{
    // initialize digital pin LED_BUILTIN as an output.
    //Clear the EEPROM, for debugging's sake
    //  for (int i = 0 ; i < EEPROM.length() ; i++) {
    //    EEPROM.write(i, 0);
    //  }
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(BUZZER, OUTPUT); // Set BUZZER - pin 9 as an output
    pinMode(BUZZER2, OUTPUT);
    pinMode(YELLOW_LIGHT,OUTPUT); //SET YELLOW LIGHT which indicates the stop button has been pressed.
    pinMode(STOP_BUTTON,INPUT);
    Wire.begin();
    Wire.beginTransmission(MPU_ADDR);
    Wire.write(0x6B); // PWR_MGMT_1 register
    Wire.write(0);    // set to zero (wakes up the MPU-6050)
    Wire.endTransmission(true);
    Serial3.begin(9600);
    Serial.begin(9600);
    Serial2.begin(9600);
}
// function that returns true, if our logic tells us the user is doing a movement
// the actual logic will change a lot as the accelerometer is examined
// the loop function runs over and over again forever
void loop()
{
    //---------------------------CONST----------------------------
    pinMode(YELLOW_LIGHT,HIGH);
    const int PROGRAM_SIZE=50;
    unsigned long timerLimit = 0;
    int32_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ, firstXMeasurement, firstYMeasurement, firstZMeasurement, firstGyXMeasurement, firstGyYMeasurement, firstGyZMeasurement, programsize;
    //To compare X dominating
    //program: the received 50 bytes
    int program[PROGRAM_SIZE];
    //progress: the 50 bytes we're sending, they tell us how much progress we've made
    int progress[PROGRAM_SIZE];
    //A control value, sets to the current time since program started in milliseconds, set each loop
    unsigned long time = millis();
    //We check a bunch of times with the delay of 0.5ms if we're moving,not moving X times in a row means we trigger pause
    unsigned int timerCounter = 0;
    //Checks if we've received a program for today
    boolean inMem = false;
    programsize = 0;
    unsigned long pause = 0;
    //Reps performed, how many reps have we performed in this session, we save this value inside of EEPROM/SDCard
    int repsPerformed = 0;
    //Memory pointer for received bytes
    int receivedBytes = 0;
    //How many pauses we've had, used to increment the EEPROM address so we're not always saving on the same spot
    int pauseCounter = 0;
    //A counter for how many times we've done nothing!
    //empric first value of a resting MPU6
    int lowerXBorder = 0;
    int upperXBorder = 0;
    int lowerYBorder = 0;
    int upperYBorder = 0;
    int lowerZBorder = 0;
    int upperZBorder = 0;
    const int memoryMax = 4000;
    int repBorderConstantX=0;
    int repBorderConstantY=0;
    int repBorderConstantZ=0;
    //Used for the first value to be extracted only once in the loop
    //Research showed first value is usually much higher than it usually is
    bool bootup = true;
    bool concentric = false;
    bool stopButtonPress = false;
    //We block the reading from serial3 when we're sending data
    bool block = true;
    Serial.print("Starting...");
    Serial.println("Bluetooth getting it?");
    //Used for getting bytes sent to use over bluetooth
    //incoming byte is the current byte we've received
    //previousByte is the previous value, this is used to check for a 0 and a following 0
    //This is beacause the pause value is stored in 2 bytes
    //extra bytes is the value of extra bytes we send that ISNT THE PROGRAM, right now we send 2 bytes for pause and one for timerCountdown
    //The third byte is 5 ... and it means how many seconds we give it before the forced timeout
    //The 4th byte is a flag to tell the program if we want to record our accelerometer data and send it back
    boolean sendAccelData = false;
    int extraBytes = 4;
    byte incomingByte = 0;
    byte previousByte = 1;
    //A boolean value, that gets triggered when we recognize that a movement has been made
    //Is used as a flag to track every accelerometer reading since start of movement to end
    bool firstMove = false;
    tone(BUZZER2, 500); // Send 0.5KHz sound signal...
    delay(4000);
    noTone(BUZZER2);
    while (true)
    {
	int stopButton = 4;
        //We don't need to listen in on serial to send to serial3
        //If we haven't read from serial3 yet, read now
        if (Serial2.available() > 0)
        {
            //This is to check that that we already received the program and loaded it
            if (!inMem)
	    {
                Serial.println("Printing from serial3");
                previousByte = incomingByte;
                incomingByte = Serial2.read();
                program[receivedBytes] = incomingByte;
                EEPROM.write(receivedBytes, incomingByte);
                receivedBytes++;
                Serial.println(incomingByte, DEC);
                //if we got a byte with the value 0 it means we've received the entire program
                Serial.print("Incoming");
                Serial.println(incomingByte);
                Serial.print("Previous");
                Serial.println(previousByte);
                if (incomingByte == 0)
		{
                    if (previousByte == 0)
		    {
                        Serial.println("In mem complete");
                        inMem = true;
                        programsize = receivedBytes;
                        delay(150);
                        tone(BUZZER, 4000);
                        delay(150);
                        noTone(BUZZER);
                    }
                    //2 because the first 2 bytes are of our pause
                    if (receivedBytes == 2)
		    {
                        //Add up both bytes to get the actual pause value
                        pause = incomingByte + previousByte;
                        Serial.println("Adding pauses together");
                    }
                }
		//Timer limit is an arbitrary number that sets up a countdown,
		//If the countdown is done without any movement being done before it, the set ends!
                if (receivedBytes == 3)
		{
                    timerLimit = incomingByte;
                    Serial.print("Timer limit: ");
                    Serial.println(timerLimit);
                }
		//If we got a "1" value byte on the 4th byte, we have to send back data instantly
		//If we got a "0" value then we don't have to send anything.
		if (receivedBytes == 4)
		{
		    if (incomingByte == 1)
		    {
			sendAccelData = true;
		    }
		    else
		    {
			sendAccelData = false;
		    }
		    Serial.println("sendAccellData");
		    Serial.println(sendAccelData);
		}
            }
        }
        //This means we're done with the program and should stop looping
        //To warn the end user, we will do a less annoying thing than BUZZERs such as LED LIGHTS
	//Don't forget about the last 2 zeroes
        if (pauseCounter == programsize - extraBytes - 2)
	{
            while (true)
	    {
                Serial.println("Program finished, reset the device");

            }
        }
	//I dont want to indent
	if(inMem)
	{
	    //Read value from the button
	    //HIGH = 1
	    //LOW = 0
	    //UNDEFINED = 3 which we use to separate between checked and unchecked values
	    //DEFAULT = 4 which is the first initialized value
	    if(bootup)
	    {
		//Means we've yet to start measuring
		stopButton = digitalRead(STOP_BUTTON);
	    }
	    else
	    {
		//Means we've started measuring already, so we dont have to know button state
		stopButton = 3;
	    }
	//The program has been loaded into memory
	}
	//We check if we've either measured already(which means we dont have to check for button state) or for button state, which has to be HIGH to start measuring
        if (stopButton == 3 || stopButton == HIGH)
	{
	    Serial3.flush();
	    Serial2.flush();
            //Initialize MPU
            Wire.beginTransmission(MPU_ADDR);
            Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
            Wire.endTransmission(false);
            Wire.requestFrom(MPU_ADDR, 14, true); // request a total of 14 registers
            //Read values out of the MPu6050
            AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
            AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
            AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
            Tmp = Wire.read() << 8 | Wire.read(); // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
            GyX = Wire.read() << 8 | Wire.read(); // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
            GyY = Wire.read() << 8 | Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
            GyZ = Wire.read() << 8 | Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
            //Map so we get smaller values
            AcX= map(AcX, -32768, +32767, -250, +250);
            AcY = map(AcY, -32768, +32767, -250, +250);
            AcY = map(AcZ, -32768, +32767, -250, +250);
            if (bootup && AcX != 0)
            {
		tone(BUZZER,3000);
		tone(BUZZER2,3000);
		delay(300);
		noTone(BUZZER);
		noTone(BUZZER2);
		delay(700);
                //X measurements are usually around 17200(decided by looking at the serial plotter)
                firstXMeasurement = AcX;
                //Y measurements are usually between -800 and -500
                firstYMeasurement = AcY;
                //Z measurements are usually between 400 and 1200
                firstZMeasurement = AcZ;
                //border for what constitutes a rep, the higher the divided constant is the harder it is to recognize a rep
                repBorderConstantX = firstXMeasurement/6;
                repBorderConstantY = firstYMeasurement/6;
                repBorderConstantZ = firstYMeasurement/6;
                //Lower and upper borders are decided based on the standard deviation of the sensor
                //Lower border denotes the eccentric movement, upper border denotes the concentric movement
                lowerXBorder = firstXMeasurement - firstXMeasurement/10;
                upperXBorder = firstXMeasurement + firstXMeasurement/10;
                lowerYBorder = firstYMeasurement - firstYMeasurement/10;
                upperYBorder = firstYMeasurement + firstYMeasurement/10;
                lowerZBorder = firstZMeasurement + firstZMeasurement/10;
                upperZBorder = firstZMeasurement + firstZMeasurement/10;
                firstGyXMeasurement = GyX;
                firstGyYMeasurement = GyY;
                firstGyZMeasurement = GyZ;
                bootup = false;
            }
            int xcomparison = abs(AcX - firstXMeasurement);
            int ycomparison = abs(AcY - firstYMeasurement);
            int zcomparison = abs(AcZ - firstZMeasurement);
            int gXcomparison = abs(GyX - firstGyXMeasurement);
            int gYcomparison = abs(GyY - firstGyYMeasurement);
            int gZcomparison = abs(GyZ - firstGyZMeasurement);
            //X is the dominating plane, if it's bigger than both the other ones combined
            //We have empirically noticed that the X gyroscope measurements are the largest when moving back and forth over the Y axis
            //Z axis seems to have almost identical X accelerometer measurements as when we move in the X direction.
            //watch out for integer wrap around
            //The user is moving
            //The X axis needs to be the dominating one for us to count it
            //Logic: if X changes a lot and so does the position then we performed a movement
            //If X changes a lot and so does Y or Z then we're most likely performing a non-rep movement
            //If AcX = 0 means the sensor is disconnected or dead.
            digitalWrite(LED_BUILTIN, HIGH);
            if (firstMove)
            {
		//Increment by 6 as we're saving 5 variables and sending them
		//This is saved into the memory so we can send it if we want to
                int tmp = tmp + 6;
		Serial.println("Ready to write to memory");
                if (tmp < memoryMax)
                {
		    //We save to EEPROM regardless, because why not
		    //Make sure to save better, currently we save 64 bit integers into per-byte basis
		    //TODO: fix incrementation of tmp
                    EEPROM.write(tmp, AcX);
                    EEPROM.write(tmp+1,AcY);
                    EEPROM.write(tmp+2,AcZ);
                    EEPROM.write(tmp+3,GyX);
                    EEPROM.write(tmp+4,GyY);
                    EEPROM.write(tmp+5,GyZ);
		    Serial.println("Writing to EEPROM");
		    //If we've enabled sending data back, do it, this is a simple 0 or 1 byte value.
		    if(sendAccelData==1)
		    {
			block = true;
			Serial3.println(AcX);
			Serial3.println(AcY);
			Serial3.println(AcZ);
			Serial3.println(GyX);
			Serial3.println(GyY);
			Serial3.println(GyZ);
			Serial.println("Sending acceleration data");
			block = false;
		    }
                }
            }
            if (((xcomparison > repBorderConstantX) || (ycomparison > repBorderConstantY) || (zcomparison > repBorderConstantZ)) && (gXcomparison > 1500 || gYcomparison > 1500 || gZcomparison > 1500) && ( AcX != 0))
            {
                //Every concentric movement needs to be followed by an eccentric movement
                //Check for first value, at the start we dont know if we do concentric or eccentric
                //After we've made sure this isnt our first repetition, we can safely start checking for the opposite value!
                if ((AcX < lowerXBorder || AcY < lowerYBorder || AcZ < lowerZBorder) && (concentric || repsPerformed == 0))
                {
		    block = true;
                    concentric = false;
                    repsPerformed++;
                    Serial.print("\n\nRep number increased ");
                    Serial.print("Eccentric");
                    Serial.print(" Reps ");
                    Serial.print(repsPerformed);
                    timerCounter = 1;
		    time = millis();
                    firstMove = true;
                    tone(BUZZER2, 3000);
                    delay(250);
                    noTone(BUZZER2);
		    Serial3.println(AcX);
		    Serial3.println(AcY);
		    Serial3.println(AcZ);
		    Serial3.println(GyX);
		    Serial3.println(GyY);
		    Serial3.println(GyZ);
		    Serial.println(AcX);
		    Serial.println(AcY);
		    Serial.println(AcZ);
                    delay(600);
		    tone(BUZZER,300);
		    delay(50);
		    noTone(BUZZER);

                }else if ((AcX > upperXBorder || AcY > upperYBorder || AcZ > upperZBorder) && (!concentric || repsPerformed == 0))
                {
                    concentric = true;
                    repsPerformed++;
                    Serial.print("\n\nRep number increased ");
                    Serial.print("Concentric");
                    Serial.print(" Reps ");
                    Serial.print(repsPerformed);
                    timerCounter = 1;
		    time = millis();
                    firstMove = true;
                    tone(BUZZER, 5000);
                    delay(250);
                    noTone(BUZZER);
		    Serial3.println(AcX);
		    Serial3.println(AcY);
		    Serial3.println(AcZ);
		    Serial3.println(GyX);
		    Serial3.println(GyY);
		    Serial3.println(GyZ);
                    delay(600);
		    tone(BUZZER,300);
		    delay(50);
		    noTone(BUZZER);

                }
                Serial.print("\nAccelerations: ");
                Serial.print(AcX);
                Serial.print(" ");
                Serial.print(AcY);
                Serial.print(" ");
                Serial.print(AcZ);
                Serial.print("\nComparisons at");
                Serial.print(xcomparison);
                Serial.print(" ");
                Serial.print(ycomparison);
                Serial.print(" ");
                Serial.print(zcomparison);
                Serial.print("\nGyros ");
                Serial.print(GyX);
                Serial.print(" ");
                Serial.print(GyY);
                Serial.print(" ");
                Serial.print(GyZ);
                Serial.print("\nGyros COMPARISONS ");
                Serial.print(gXcomparison);
                Serial.print(" ");
                Serial.print(gYcomparison);
                Serial.print(" ");
                Serial.print(gZcomparison);
                digitalWrite(LED_BUILTIN, LOW);
            }
                //The user is not moving
                //Make this so it only starts timing out if we started a movement
                //Once the timer has been set to 1 it means we've recognized a movement
            else
            {
                if (timerCounter >= 1)
                {
		    //Current time, used to check for differences between time since the start of loop
		    unsigned long currentTime = millis();
		    if((currentTime - time) >= timerLimit*1000){
			timerCounter = timerLimit + 1;
			Serial.println("Forced timeout");
			Serial.println((currentTime - time));
		    }
                }
            }
            if (repsPerformed == 1 && timerCounter > timerLimit - 1)
            {
                Serial.println("Recognized accidental movement, restarting thing");
		tone(BUZZER,3000);
		delay(300);
		noTone(BUZZER);
                //If there was only 1 rep, remember odd numbers mean imperfect reps performed, we most likely triggered it by accident
                repsPerformed = 0;
                timerCounter = 0;
                bootup = true;
                firstMove = false;
            }
            if (Serial2.available() > 0)
	    {
                //The program is already in memory, so now all we do is listen for skip/repeat of a set
                //Skip: byte 255
                //Repeat: byte 0
                //Warning, sending the program again will skip, as it contains a 0
                byte controlByte = Serial2.read();
                Serial.print("Receiving ");
                Serial.println(controlByte);
                if (controlByte == 255)
		{
                    Serial.println("Skipping");
                    timerCounter = timerLimit + 1;
                }
                //Can't decrement value 0
                Serial.println(pauseCounter);
	    //Include if button2 = HIGH
		if (controlByte == 0 && repsPerformed != 0)
		{
		    Serial.println("Repeating during a set");
		    repsPerformed = 0;
		    timerCounter = 0;
		    tone(BUZZER,6000);
		    pauseCounter--;
		    delay(1500);
		    noTone(BUZZER);
		    //TRIGGER LIGHT 2 and 3
		}
		//Include if button3=HIGH
		else if (controlByte == 0 && pauseCounter > 0)
		{
		    Serial.println("Repeating");
		    repsPerformed = 0;
		    tone(BUZZER,3000);
		    pauseCounter--;
		    delay(1500);
		    noTone(BUZZER);
		    //TRIGGER LED 4
		}
            }
	    //Timer counter is a value that starts getting incremented until it reachess a value sent to use over bluetooth
	    //When it does it cancels the set as the puase is too long
	    //Make incrementation to timerLimit last actual seconds instead of an unnown length of time, use MILLIS();
            if (repsPerformed == program[pauseCounter + extraBytes] * 2 || timerCounter > timerLimit)
            {
                Serial.println(program[pauseCounter]);
                Serial.println("Pause triggered ");
                //Trigger BUZZER
                Serial.println("Triggering BUZZER");
                tone(BUZZER, 2000);
                delay(4000);
                noTone(BUZZER);
                timerCounter = 0;
                firstMove = false;
                // read a byte from the current address of the EEPROM
                //make sure the value isnt over 255, but its most likely not
                //Every address in EEPROM takes in 1 byte, so 0-255 in values
                //Reasonably none of the numbers will reach 255 but we still decide to
                //MEGA ADK has about 4kB space, reasonably we expect we wont save more than 300 numbers and we will use the rest for the program and pause value
                //Pause value in this case will need 2 bytes, we can reasonably expect someone wants more than 255seconds of rest, encoding the values to efficiently spread out is not worth the added complexity
                //addr,val

                if (repsPerformed < 255 && pauseCounter < 300)
                {
                    EEPROM.write(pauseCounter, repsPerformed);
                }
                else
                {
                    //TODO: Decide how to warn the user of overflowing his allocated space
                    //Probably done in the app
                }
                pauseCounter++;
                if (pauseCounter >= programsize - extraBytes)
                {
                    tone(BUZZER, 2000);
                    tone(BUZZER2, 5000);
                    delay(5000);
                    noTone(BUZZER);
                    noTone(BUZZER2);
                    Serial.println("Done");
		    //When its finished, send over the program completed reps.
		    //UNroundable numbers are already taken care of
                    for (int i = 1; i < programsize + 1; i++) {
                        Serial3.println(progress[i] / 2);
			//We send a 0 after each number so we're able to separate them
                    }
                    //The first element is the pause length
                    delay(pause * 1000);
                }
                Serial.println("On set");
                Serial.println(pauseCounter);
                //Put it back to 0 after we're done writing in the value in EEPROM
                //If the number of recorded reps is an uneven number, we round uup
		//Make sure we're not over the limit, just in case, user should be notified
		if (pauseCounter < PROGRAM_SIZE){
		    progress[pauseCounter] = repsPerformed;
		}
                if (repsPerformed % 2 != 0)
                {
                    repsPerformed++;
                }
                repsPerformed = 0;
                Serial.println("Pausing");
                Serial.println(pause);
                delay(pause * 1000);
                //Blink 5 times
                for (int i = 0; i < 5; i++)
                {
                    tone(BUZZER, 1000);
                    tone(BUZZER2, 1000);
                    digitalWrite(LED_BUILTIN, HIGH);
                    delay(1000);
                    digitalWrite(LED_BUILTIN, LOW);
                    delay(500);
                    Serial.write("Pause over! ");
                    noTone(BUZZER);
                    noTone(BUZZER2);
                }
                Serial.write("Looping");
		tone(BUZZER,7000);
		delay(600);
		noTone(BUZZER);
		bootup = true;
            }
        }
    }
}

