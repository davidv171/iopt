package com.iopt.davidv7.iopt;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class IoPT extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    //Number of days we get from the template
    private int numberOfDays = 0;
    //UUID of the bluetooth device
    //Custom service UUID:
    static final UUID bUUID = UUID.fromString("0000FFE0-0000-1000-8000-00805F9B34FB");
    static final UUID charUUID = UUID.fromString("0000FFE0-0000-1000-8000-00805F9B34FB");
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    //Button used to send data for debugging purposes
    private BluetoothAdapter bAdapter;
    private BluetoothManager bManager;
    private ViewPager mViewPager;
    private String VTAG = "VB";
    private String TAG = "DBG";
    private int[] sendData;
    //bGatt might be the client!
    //In the example app, we don't need to be the server to send and read
    private BluetoothGatt bGatt;
    private TextView rcv;
    private File file;
    private FileOutputStream fos;
    private EditText pauseTimer;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_io_pt);
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.BLUETOOTH_ADMIN)
                != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG,"No permission for bluetooth");
            // Permission is not granted
        }
        else{
            Log.d(TAG,"Bluetooth permission granted");
        }
        bManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bAdapter = bManager.getAdapter();
        rcv = findViewById(R.id.rcv);
        file = new File(getApplicationContext().getFilesDir(),"file.txt");
        try{
            fos = openFileOutput("file.txt",Context.MODE_APPEND);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final BluetoothDevice[] bDevice = new BluetoothDevice[1];
        // Ensures Bluetooth is available on the device and it is enabled. If not,
// displays a dialog requesting user permission to enable Bluetooth.
        if (bAdapter == null || !bAdapter.isEnabled()) {
            requestBluetoothEnable();
        }
            scanBluetooth(bAdapter,bDevice);

        numberOfDays = 6;


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        //Delete tabs based on number of days!
        for (int i = 6; i > numberOfDays - 1; i--) {
            tabLayout.removeTabAt(i);
        }
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Increment training max value by user generated increment value
                sendData = new int[]{255};
                setupClient(bAdapter);
            }
        });
        pauseTimer = findViewById(R.id.textView3);
        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Increment training max value by user generated increment value
                Log.d(TAG,"Sending boi");
                //{pause,pause,timer countdown,send accelerometer data back, the rest is the amount of reps of the program, last 2 zeros are delimiters
                int pause = Integer.parseInt(pauseTimer.getText().toString());
                int secondPause = 0;
                if(pause > 255){
                    secondPause = pause - 255;
                    pause = 255;

                }
                sendData = new int[]{pause, secondPause, 5, 1, 5, 3, 2, 0, 0};
                setupClient(bAdapter);
            }
        });
        FloatingActionButton fabrepeat = (FloatingActionButton) findViewById(R.id.floatingActionButtonRepeat);
        fabrepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Increment training max value by user generated increment value
                sendData = new int[]{0};
                setupClient(bAdapter);

            }
        });

    }

    private boolean scanBluetooth(final BluetoothAdapter bAdapter, final BluetoothDevice[] bDevice) {
        final BluetoothLeScanner bScanner = bAdapter.getBluetoothLeScanner();
        Log.d(TAG,"Started scanning");
        final boolean[] stopCallback = {false};
        if(!stopCallback[0]){
            bScanner.startScan(new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    super.onScanResult(callbackType, result);
                    System.out.println("STOP CALLBACK" + stopCallback[0]);
                    bDevice[0] = result.getDevice();
                    System.out.println("BDEVICE ADD" + bDevice[0].getAddress());
                    if (bDevice[0].getName() == null || stopCallback[0]) {
                        Toast.makeText(getApplicationContext(), "Didn't find a device", Toast.LENGTH_LONG);

                    } else if (bDevice[0].getName().equals("MLT-BT05")){
                        stopCallback[0] = true;
                        System.out.println("ADDRESS" + bDevice[0].getName());
                        bGatt = bDevice[0].connectGatt(getApplicationContext(), true, new BluetoothGattCallback() {
                            @Override
                            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                                super.onConnectionStateChange(gatt, status, newState);
                                if(status == BluetoothGatt.GATT_SUCCESS){
                                    Log.d(TAG, "Successful GATT " + newState);
                                    //Simply triggers a discoverService method, which in return triggers onServiceDiscovered, which writes sample data into the characteristic
                                    setupClient(bAdapter);
                                    return;

                                }
                                if(status == BluetoothGatt.GATT_FAILURE){
                                    Log.e(TAG,"Bigg error boi");
                                    System.exit(1);
                                }
                                else{
                                    Log.w(TAG,"Oh no, gatt failed " + status + " retrying");
                                    return;
                                }

                            }
                            @Override
                            //Triggered after we write data in a characteristic
                            public void onCharacteristicWrite(BluetoothGatt gatt,
                                                              BluetoothGattCharacteristic characteristic, int status) {
                                Log.d(TAG, "Characteristic written" + status);
                            }
                            //Triggered when someone(the remote device if everything is set up correctly) changes a characteristic!

                            public void onCharacteristicChanged(BluetoothGatt gatt,
                                                                BluetoothGattCharacteristic characteristic) {
                                Log.w(TAG,"Characteristic changed");
                                try {
                                    Log.d(TAG,"Changed characteristic" + new String(characteristic.getValue(),"UTF-8"));
                                    try{
                                        rcv.append(characteristic.getStringValue(0));
                                        try {
                                            fos.write(characteristic.getStringValue(0).getBytes());
                                        } catch (IOException e) {

                                        }

                                    }
                                    catch(IndexOutOfBoundsException err){
                                        Log.e(TAG," Index out of bounds");
                                    }
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                                Log.d(TAG,"Size of discovered" + gatt.getServices().size());
                                Log.d(TAG,"Request MTU success: " + gatt.requestMtu(50));

                            }
                            @Override
                            public void onMtuChanged(BluetoothGatt gatt, int mtu, int status){
                                getData(gatt);
                                writeCharacteristic(gatt);
                                Log.d(TAG,"Services discovered");
                            }


                        }, BluetoothDevice.TRANSPORT_LE);
                        Log.d(TAG, "Bgatt value:" + bGatt);


                    }
                }
            });
        }

        if(bGatt==null){
            Log.w(TAG, "BGATT IS NULL WHILE SCANNING");
        }
        Log.d(TAG, "Finished scanning...");
        return true;
    }
    //Doesn't do much, starts discovering services, which triggers onServicesDiscovered
    private void setupClient(BluetoothAdapter bAdapter) {
        Log.d(TAG, "Started setting up server");

        if (bAdapter != null) {
            Log.d(TAG, "SERVICE DISCOVERED" + bGatt.discoverServices());

        }
    }
    //Write characteristic into the discovered service
    //There is only one characteristic, so we can just take the first index
    private void writeCharacteristic(BluetoothGatt gatt){
        BluetoothGattService bService = gatt.getService(bUUID);
                            BluetoothGattCharacteristic bChars = bService.getCharacteristics().get(0);

                            if(bChars != null){
                                //0 delimits the last workout of the program
                                byte[] bytes = new byte[sendData.length];
                                for(int i = 0; i < sendData.length;i++){
                                    bytes[i] = (byte) sendData[i];
                                }
                                bChars.setValue(bytes);
                                bService.addCharacteristic(bChars);
                                boolean success = gatt.writeCharacteristic(bChars);
                                if(success){
                                    Log.d(TAG, "Write characteristic successful");
                                }
                                else{
                                    Log.e(TAG, "Write characteristic failed");
                                }
                            }
                            else{
                                Log.e(TAG,"Characteristic not found");
                            }
    }
    //Set up notifications for the discovered service!
        private void getData(BluetoothGatt gatt){

            BluetoothGattService bService = gatt.getService(bUUID);
            BluetoothGattCharacteristic bChars = bService.getCharacteristics().get(0);
            gatt.setCharacteristicNotification(bChars,true);
        }
    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        Log.d(TAG, "Requested user enables Bluetooth. Try starting the scan again.");
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.e(TAG,"Missing BLE");
            Toast.makeText(getApplicationContext(),"No ble support", Toast.LENGTH_LONG);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_io_pt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
        private TextView rcv;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_io_pt, container, false);
            rcv = rootView.findViewById(R.id.rcv);

            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                default:
                    System.out.println("How did you get here?");
            }
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            int numberOfDays = 6;
            return numberOfDays;
        }
    }
}
