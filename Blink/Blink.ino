/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}
// function that returns true, if our logic tells us the user is doing a movement
// the actual logic will change a lot as the accelerometer is examined
boolean isMoving(int xAxis, int yAxis, int zAxis){
    if(xAxis < 0.5 && yAxis > 0.5 && zAxis > 0.5){
      return true;
    }
    else{
      return false;
    }
}
// the loop function runs over and over again forever
void loop() {
  //LOGIKA: Utripaj 15x z 1.5 skupnim sekundnim delayem nato počakaj 90 sekund 
  int timerCounter = 0;
  const int pause = 90000;
  //Reps performed, how many reps have we performed in this session, we save this value inside of EEPROM/SDCard
  int repsPerformed = 0;
  while(1){
      int xAxis,yAxis,zAxis;
      digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(1000);                       // wait for a second
      digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
      delay(500);
      timerCounter++;
      //Pause is over, wait for user to start doing movement
      if(timerCounter == 15){
          digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
                                             //TODO: Get accelerometer data, only trigger pause delay if no movement is being done
                            f fqwagagagq
                            //TODO: Don't count repetitions until pause is over
          delay(pause);                      // wait for 90 seconds
          timerCounter = 0;
      }
      //TODO: This is the logic for where movement is done, use the function for movement checking
  }

}
